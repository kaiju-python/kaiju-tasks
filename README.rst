
.. image:: https://badge.fury.io/py/kaiju-tasks.svg
    :target: https://pypi.org/project/kaiju-tasks
    :alt: Latest package version

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style - Black

- `Python <https://www.python.org>`_ >=3.11
- `Postgres <https://www.postgresql.org>`_ >=14
- `Redis <https://redis.io>`_ >=7.0

System and user tasks automation.
It allows you to schedule RPC commands execution across multiple applications, gather results, chain multiple commands
and share data between them. See `documentation <https://kaiju-tasks.readthedocs.io/>`_ for more info.

Installation
------------

pip install kaiju-tasks
