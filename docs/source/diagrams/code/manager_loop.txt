participant "Task manager" as Manager
database "DB" as Table
database "Redis" as Redis

frame manager loop

group expell_dead_executors
  note over Manager, Redis: Executors what missed three consequent ping\nintervals will be expelled, their tasks will be suspended.
  Manager->Redis:Get executor lifetimes
  Manager<--Redis:{id: unix_time, ...}
  note over Manager, Redis: Suspend tasks of not responding executors,\nset executor_id null.
  Manager->Table:Set SUSPENDED
  Manager->Redis:Remove dead executors
end

group abort_timed_out_tasks
note over Manager, Table: Fail all timed out tasks not aborted\nby executors, set exit code 130 and\nexecutor_id null.
Manager->Table:Set FAILED
end

group restart_cron_tasks
  note over Manager,Table: Set FINISHED or FAILED cron tasks to IDLE,\nit does not include failed tasks with retries since\nthey are restarted by queue_tasks procedure.
  Manager->Table:Set IDLE
end

group queue_tasks
  note over Manager,Redis: Queue SUSPENDED, IDLE and FAILED tasks with restarts.
  Manager->Table:Set QUEUED
  Manager<--Table:Task []
  loop for Task in Task []
      Manager->Redis:RPC executor.run_task(Task)
  end
end

== sleep unitl next cycle ==