**services** - application services
===================================

.. autoclass:: kaiju_tasks.services.TaskService
   :members:
   :exclude-members: table, ErrorCode, Permission, PermissionKeys, permissions, routes, validators
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_tasks.services.NotificationService
   :members:
   :exclude-members: table, ErrorCode, Permission, PermissionKeys, permissions, routes, validators
   :show-inheritance:
   :inherited-members:

.. autoclass:: kaiju_tasks.services.TaskManager
   :members:
   :exclude-members: init, close, closed, routes, permissions, validators
   :show-inheritance:

.. autoclass:: kaiju_tasks.services.TaskExecutor
   :members: __init__, run_task
   :show-inheritance:
