.. include:: readme.rst

Guides
------

.. toctree::
   :maxdepth: 1

   user_guide
   operation
   dev_guide

Library Reference
-----------------

.. toctree::
   :maxdepth: 1

   kaiju_tasks.services
   kaiju_tasks.types

.. include:: license.rst
