**types** - data types and constants
====================================

.. autoclass:: kaiju_tasks.types.TaskCommand
   :members:

.. autoclass:: kaiju_tasks.types.Message
   :members: max_timeout
   :exclude-members: __init__, __new__

.. autoclass:: kaiju_tasks.types.Timer
   :members: timer
   :exclude-members: __init__, __new__

.. autoclass:: kaiju_tasks.types.Task
   :members:

.. autoclass:: kaiju_tasks.types.Notification
   :members:

.. autoclass:: kaiju_tasks.types.Limit
   :members:

.. autoclass:: kaiju_tasks.types.TaskStatus
   :members:

.. autoclass:: kaiju_tasks.types.ExitCode
   :members:

.. autoclass:: kaiju_tasks.types.RestartPolicy
   :members:
